class Artifact extends UX {

  constructor(def, board) {
    super();
    this.name = def.name;
    this.status = false;
    this.engine = new def.engine(def, board);
    this.board = board;
    this.htmlNode = this.engine.templateInsert();
    this.htmlNode.id = def.name;
    this.htmlNode.style.display = 'none';

    if (this.htmlNode.querySelector('#jxgbox')) {
      this.htmlNode.querySelector('#jxgbox').id = this.name + "_board";
    };

    this.htmlNode.classList.add(...def.style?.class ?? '');
  };

  initArtifact = () => {

    if (this.status) { return; }
    this.status = true;
    if (this.htmlNode.querySelector('#jxgbox')) {
      this.htmlNode.querySelector('#jxgbox').id = def.name + '_board';
    }
    this.htmlNode.style.display = 'flex';
    this.addEvents();

  };

  addEvents() {

    this.allbtn = this.htmlNode.querySelector('.all-btn');
    this.allbtn.addEventListener('click', (e) => {

      const button = e.target;
      if (button.classList.contains('check')) {

        const data = this.engine.validate();
        if (data.status != 3) {
          this.visibleModal(true, data);
        }

      } else if (button.classList.contains('reset')) {

        console.log('reset', this.engine.reset());

      } else if (button.classList.contains('return')) {

        console.log('return', this.engine.return());

      };
    });

  }

  initEngine = () => {
    this.engine.initEngine(this.def, this.board);
  };
};