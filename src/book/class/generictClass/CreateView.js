class CreateView {
  constructor(allDef, defBoard) {

    this.defBoard = defBoard;
    this.allArtifacts = [];
    if (allDef) {
      this.initVIew(allDef);
    }

  }

  initVIew = (allDef) => {

    const { artifacts, parent, style } = allDef ?? {};
    const fragment = new DocumentFragment();
    if (!allDef || Object?.keys(artifacts).length <= 0) {
      return;
    }

    Object.keys(artifacts).forEach(def => {
      const artifact = artifacts[def];
      const artifactDef = { name: def, parent, ...artifact, style, board: this?.defBoard?.[artifact?.board] }

      return this.addArtefact(
        artifactDef,
        fragment);

    });


    let container = document.querySelector(`.${parent}, #${parent}`);

    container = container ?? document.querySelector(`.main`);
    if (container) {
      container.appendChild(fragment);
    } else {
      document.body.appendChild(fragment);
    }
    this.initArtifats();
  };

  addArtefact = (artifact, fragment) => {

    const { parent } = artifact;
    console.log(parent);
    const artClass = new Artifact(artifact, artifact.board);

    if (!parent && fragment) {
      fragment.appendChild(artClass.htmlNode);
    } else {
      let container = document.querySelector(`#${parent},.${parent}`);
      container = container ?? document.querySelector(`.main`);
      if (container) {
        container.appendChild(artClass.htmlNode);
      } else {
        document.body.appendChild(artClass.htmlNode);
      }
    }
    this.allArtifacts.push(artClass);
    return artClass;
  };

  initArtifats = () => {
    this.allArtifacts.forEach((artifact) => {
      if (!artifact.status) {
        artifact.initArtifact();
        artifact.initEngine();
      };
    });
  };
}