class HorizontalSegment extends baseBoards {

  constructor(def, defBoard) {

    super(defBoard);
    this.allPoints = [];
    this.InputTarget = 0;
    this.targetInterval = 0;
    this.defBoard = defBoard;
    this.idTemplate = def?.template?.id ?? 'temp-segment';
    this.conditions = def?.conditions ?? {};
    this.idboard = def.name + "_board";
    this.htmlNode = def?.template?.node ?? null;
    this.validation = new ValidationHorizontal(this.def);
    this.templateInsert();
    this.intervals = [];
  }

  templateInsert = () => {
    if (!document.querySelector('#temp-buttons')) {

      const buttonsTemplate = ` <template id = "temp-buttons">
        <button class="buttonTool styleBtn buttonTertiary" title = "value 1"> 1
        </button>
        </template> `;
      this.buttonsTemplate ??= buttonsTemplate;

      document.body.insertAdjacentHTML('afterend', this.buttonsTemplate);
      this.tmpButton = document.querySelector('#temp-buttons');
    }
    if (!document.querySelector('#temp-segment')) {

      const $templateDefaults = `<template id="temp-segment"><
        <div class=" artifacBase artifactShort">
          <div class="statement-top border-board-dark w-100 textCenter"></div>
    
          <div id="jxgbox" class="boardVerticalShort border-board-dark"></div><!-- board donde tiene que ir el-->
          <div class="statement statement-bottom border-board-dark w-100 h-100 textCenter mt-1 mb-1"
            style="height: 100%; min-height: 30px; display: none;"></div>
    
          <div class="all-btn w-100 border-board-dark">
            <div class="btnBaseArtifact border-dark rounded">
              <div class="sectionBtn interactive-btn"> </div>
              <div class="sectionBtn default-btn gap-2">
                <button class="reset styleBtn buttonSecundary" title="Reset"></button>
                <button class="check styleBtn buttonPrimary" title="Validar"></button>
              </div>
            </div>
          </div>
        </div>
    
      </template>`;


      this.template ??= $templateDefaults;
      document.body.insertAdjacentHTML('afterend', this.template);
    };

    this.htmlNode ??= document.querySelector('#' + this.idTemplate).content.firstElementChild.cloneNode(true);
    return this.htmlNode;
  };

  initEngine() {

    if (this.initBoardBase({ id: this.idboard, ...this.defBoard })) {
      if (this.defBoard?.intervals) {
        this.createIntervals({ intervals: this.defBoard.intervals });
      }
    }

    if (this.conditions.keyBoard && this?.defBoard?.topButtons) {
      this.inpu;
      this.setTopButtons(this?.defBoard?.topButtons);

      this.htmlNode.querySelector('.statement-top').addEventListener('click', (e) => {
        if (!e.target.classList.contains('buttonTool')) {
          return;
        };

        this.validateStatus = false
        const interval = this.intervals[this.targetInterval];
        const mathfield = interval.inputs[this.InputTarget].mathfield;

        mathfield.value = e.target.textContent;

        if (this.InputTarget >= this.intervals[this.targetInterval].inputs.length - 1) {
          this.InputTarget = 0;
        } else {
          this.InputTarget++;
        };

        if (this.targetInterval >= this.intervals.length - 1) {
          this.targetInterval = 0;
        } else {
          this.targetInterval++;
        }

      });
      this.htmlNode.querySelector('.statement-top').style.display = 'flex';
    };


    this.initTimer();
  };

  createIntervals(params) {
    const { intervals = [] } = params;
    intervals.forEach((interval) => {
      this.addInterval(interval);
    });
  };

  addInterval({
    height = 1.5,
    interval = [-2, 2],
    inputs = { a: "", b: "", c: "" },
    fillInterval = true,
  }) {

    const lines = this.createLines({
      lines: [
        {
          points: [
            [interval[0], height],
            [interval[1], height],
          ],
          style: {
            firstArrow: true,
            lastArrow: true,
          },
        },
        {
          points: [
            [interval[0], height],
            [interval[0], 0, true],
          ],
        },
        {
          points: [
            [interval[1], height],
            [interval[1], 0, true],
          ],
        },
      ],
    });

    if (fillInterval) {
      const fillCurves = { curves: [] };
      const fillPoints = { flex: 1.5, points: [] };
      fillPoints.points.push({
        x: interval[0],
        y: 0,
        style: { visible: true },
      });

      for (
        let i = interval[0];
        i < interval[1];
        i += fillInterval?.inteval ?? 1
      ) {
        fillPoints.points.push({
          x: i + (fillInterval?.inteval ?? 1) / 2,
          y: 0.75,
          style: { visible: false },
        });
        fillPoints.points.push({ x: i, y: 0, style: { visible: true } });
      }
      fillPoints.points.push({
        x: interval[1],
        y: 0,
        style: {},
      });

      fillPoints.points.sort((a, b) => a.x - b.x);
      fillCurves.curves.push(fillPoints);

      this.createCurves({
        curves: fillCurves.curves,
        styles: { strokeWith: 8 },
      });
    };

    const newInputs = this.createInputs({
      inputs: [
        this.defineInput(interval[0], -1, inputs, 'a'),
        this.defineInput((interval[0] + interval[1]) / 2, height + 0.7, inputs, 'b'),
        this.defineInput(interval[1], -1, inputs, 'c')
      ],
    });

    if (this.conditions.keyBoard && this?.defBoard?.topButtons) {
      newInputs.forEach((e) => {
        e.mathfield.setAttribute('disabled', 'disabled');
      });
    };

    this.intervals.push({ inputs: newInputs });
    if (MathLive) {
      MathLive.renderMathInDocument();
    }
  }

  defineInput(x, y, inputs, select = 'a') {
    return {
      x, y,
      value: inputs?.[select]?.value ?? inputs?.[select],
      style: {
        mathClass: ((inputs?.[select]?.style?.mathClass ?? "") + (' inp' + select.toUpperCase())),
        mathStyle: inputs?.[select]?.style?.mathStyle ?? {},
      }
    };
  }

  setTopButtons(allButtons) {
    if (!allButtons) {
      return;
    }
    allButtons.forEach((item) => {

      const newButton = this.tmpButton.content.firstElementChild.cloneNode(true);
      newButton.textContent = item.value;
      this.htmlNode.querySelector('.statement-top').appendChild(newButton);

    });
  };
}