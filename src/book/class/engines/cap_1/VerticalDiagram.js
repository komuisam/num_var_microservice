
class DiagramVertical extends BaseEngine {
  constructor(def, id) {

    super();

    this.def = def;
    this.conditions = this.def.conditions;
    this.artId = this.def.id
    this.validation = new verticalValidate();
    this.hmlNode = null;
    this.inputsValidate = [];

  }

  templateInsert = () => {
    /* el template insert no afecta a nada a parte de que exista el template en la vista de resto no tiene por que romper el codigo */

    const tmp1 = `<template id="tmp1">
    <div class="diagramform1">
      <math-field class="mdrigth input_1 hblted diagramRound"></math-field>
      <math-field class="mdleft input_2 hblted diagramRound"> </math-field>
    </div>
  </template>`;

    const tmp2 = `<template id="tmp2">
    <div class="diagramform1">
      <math-field class="squareDiagram input_1 hblted"></math-field>
      <div class="diagramArrows">
        <div class="arrowDown"></div>
        <div class="arrowUp md"></div>
      </div>
      <math-field class="squareDiagram input_2 hblted"></math-field>
    </div>
  </template>`;

    const maintTmp = `
    <template id="temp">
      <div class="artifactShort artifacBase fixedWith">
        <div class="" id="artifact1">
        
          <div class="diagramGrid contentOthers">
            <math-field class="diagramform2 positionDiagramUp input_up hblted"></math-field>  
            <math-field class=" diagramform2 positionDiagramDown input_down hblted"></math-field>
          </div>

          <div class="all-btn w-100 border-board-dark">
          <div class="btnBaseArtifact border-dark rounded">
            <div class="sectionBtn interactive-btn"></div>
            <div class="sectionBtn default-btn gap-2">
              <button class="reset styleBtn buttonSecundary" title="Reset"></button>
              <button class="check styleBtn buttonPrimary" title="Validar"></button>
            </div>
          </div>
        </div>
      </div>
    </template>`;



    if (!document.querySelector('#temp')) {
      this.template ??= maintTmp;
      document.body.insertAdjacentHTML('afterend',
        this.template);
    }
    if (!document.querySelector('#tmp1')) {
      document.body.insertAdjacentHTML('afterend', tmp1);

    }
    if (!document.querySelector('#tmp2')) {
      document.body.insertAdjacentHTML('afterend', tmp2);
    }
    this.temp1 ??= document.querySelector('#tmp1');
    this.temp2 ??= document.querySelector('#tmp2');

    this.htmlNode ??= document.querySelector('#temp').content.firstElementChild.cloneNode(true);
    return this.htmlNode;


  };

  artiopc(def) {
    const options = {
      "1": this.temp1,
      "2": this.temp2
    };
    /* sacar el input top y bottom de for evita que se cambien los valores de estos por cada iteracion y evita tener que ponerlos en la def */
    const top = this.def?.inputs?.top;
    const bottom = this.def?.inputs?.bottom;

    const input_up = this.htmlNode.querySelector(".input_up");
    const input_down = this.htmlNode.querySelector(".input_down");
    /* con llamar el mismo metodo puedo cambiar el  valor de los elementos  */
    this.setInput(
      input_up,
      top?.style,
      top?.value ?? false
    );

    this.setInput(
      input_down,
      bottom?.style,
      bottom?.value ?? false
    );

    this.def.inputs.content.forEach((item, index) => {

      const {/* destructurar el objeto me permite tener acceso mas simple a sus propiedades */
        value1 = "",
        value2 = "",
        style1 = "",
        style2 = "",
        type = 1
      } = item;

      const clone = options[type].content.firstElementChild.cloneNode(true);
      /* dentro del for solo se tiene que interactuar con los dos inputs */
      const input_1 = clone.querySelector(".input_1");
      const input_2 = clone.querySelector(".input_2");

      this.setInput(input_1, style1, value1, type);
      this.setInput(input_2, style2, value2, type);

      switch (type) {
        /* dependiendo el tipo y si viene falso se pone none la flecha correspondiente */
        case 2:
          if (value1 === false) { clone.querySelector('.arrowDown').style.display = 'none'; }
          if (value2 === false) { clone.querySelector('.arrowUp').style.display = 'none'; }
          break;
      }

      this.htmlNode.querySelector('.contentOthers').appendChild(clone);
      this.htmlNode.id = this.artId;
    });
  };
  //cambiar las propiedades desde un metdo es mejor queda mas organizado y limpio se peude reutilizar
  setInput(input, style, value) {

    input.style = style ?? '';
    input.textContent = value ?? '';

    if (value !== undefined && value !== "") {
      input.setAttribute("disabled", "");
    } else {
      //quite la validacion del input existente
      this.inputsValidate.push(input);
    };
    //aqui cambia si el value es false indica que no se mostrara
    if (input && value === false) {
      input.style.display = 'none';
    }
  }

  iniTMainReset(def) {
    //con tener los inputs a modificar en algun lugar puedes acceder directamente para interactuar con ellos

    this.inputsValidate.forEach((inputs) => {
      if (inputs.textContent == "") {
        inputs.value = "";
        inputs.style.background = "transparent";
      }

    });

  }
  initEngine() {
    this.artiopc(this.def);
    this.initTimer();
  }
}
