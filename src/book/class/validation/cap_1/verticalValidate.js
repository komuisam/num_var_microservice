class verticalValidate {
  constructor() {

    this.color = {
      succes: "#b6f3bf",
      failed: "#eab8a5",
      forAswer: "transparent"
    };
  }

  iniTMainValidations(def, conditions,) {
    this.inputs = def.inputsValidate;
    this.condi = conditions;
    const data = {
      typeInteraccion: 'standard',
      status: 1,
      timer: 0,
      userInteraction: {},
      message: 'esta todo correcto',
      interaction: {
        correctas: 0,
        inCorrectas: 0,
        forAswer: 0
      }
    };

    const interaction = data.interaction;
    for (let i = 0; i < this.inputs.length; i++) {
      const input = this.inputs[i];
      const vlue = this.condi[i].value;

      if (input.value == "") {
        interaction.forAswer++;
      } else {
        /* aqui es mejor poner el la def un array con las respeustas correctas */
        if (vlue.includes(input.value)) {/*  con esta linea tienes la posivilidad de poner multiples respuestas */
          interaction.correctas++;
          input.style.background = this.color.succes;
          data.status = "2";
        }
        else {
          interaction.inCorrectas++;
          input.style.background = this.color.failed;
          data.message = "incorrecto";
        }
      }
    }



    return data;

  };

}