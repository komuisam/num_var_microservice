const allDef = {
  engine: DiagramVertical,
  artifacts: {
    artifact_1: {
      //parent: 'tareaContent',
      parent: 'léxicoContent',
      conditions: [
        { name: "up", value: ["1", "2"] },
        { name: "f1c2", value: "2" },
        { name: "down", value: "3" },
        { name: "f2c2", value: "4" },
        { name: "f3c1", value: "5" },
        { name: "f3c2", value: "6" },
        { name: "f4c2", value: "7" },
        { name: "f5c1", value: "8" }

      ],
      engine: DiagramVertical,
      inputs: {
        top: { value: "" },
        content: [
          { type: 1, value1: 'x' },
          { type: 2, value1: '()+3' },
          { type: 1 },
          { type: 2, value1: '2*()' },
          { type: 1, value2: '20' }
        ],
        bottom: { value: "2" },
      },
      id: "artifact1"
    },
    artifact_2: {
      parent: 'classContent',
      conditions: [
        { name: "f1c1", value: "1" },
        { name: "f1c2", value: "2" },
        { name: "down", value: "3" },
        { name: "f2c1", value: "4" },
        { name: "f2c2", value: "5" },
        { name: "f3c1", value: "6" },
        { name: "f3c2", value: "7" },
        { name: "f4c1", value: "8" },
        { name: "f4c2", value: "9" },
        { name: "f5c1", value: "10" },
        { name: "f5c2", value: "11" }
      ],
      engine: DiagramVertical,
      inputs: {
        top: { value: "3 y + 2 = 8" },
        bottom: { value: "" },
        content: [
          { type: 1 },
          { type: 2 },
          { type: 1 },
          { type: 2 },
          { type: 1 }
        ]
      },
      id: "artifact2"
    },
    artifact_3: {
      parent: 'classContent',
      conditions: [
        // {value:"7"},
        // {value:"13"},
        // {value:"8"},
        // {value:"9"},
        // {value:"2"},
        // {value:"2"},
        // {value:"2"},
        // {value:"7"}
      ],
      engine: DiagramVertical,
      inputs: {
        top: { value: "ECUACION" },
        bottom: { value: "SOLUCION" },
        content: [
          { type: 1, value1: 'incognita', value2: 'solucion' },
          { type: 2, value1: 'operacion', value2: 'operacion inversa' },
          { type: 1, value1: 'formula', value2: 'numero' }
        ]
      },
      id: "artifact3"
    },
    artifact_4: {
      parent: 'léxicoContent',
      conditions: [
        // {value:"7"},
        // {value:"13"},
        // {value:"8"},
        // {value:"9"},
        // {value:"2"},
        // {value:"2"},
        // {value:"2"},
        // {value:"7"}
      ],
      engine: DiagramVertical,
      inputs: {

        bottom: { value: "3" },
        content: [
          { type: 1, value1: 'x', value2: '3' },
          { type: 2, value1: '2*()', value2: '()/2' },
          { type: 1, value1: '2*x', value2: '6' }
        ]
      },
      id: "artifact4"
    },
    artifact_5: {
      parent: 'tareaContent',
      conditions: [
        { name: "f2c2", value: "()-2" },
        { name: "f3c1", value: "6" },
        { name: "f3c2", value: "6" },
      ],
      engine: DiagramVertical,
      inputs: {
        top: { value: "1", style: "display:none;" },
        bottom: { value: "1", },
        content: [
          { type: 1, value1: '4', value2: '4' },
          { type: 2, value1: '()+2' },
          { type: 1 }
        ]
      },
      id: "artifact5"
    },
    artifact_6: {
      parent: 'léxicoContent',
      conditions: [

      ],
      engine: DiagramVertical,
      inputs: {
        top: { value: "1", style: "display:none;" },
        bottom: { value: "1", },
        content: [
          { type: 1, value1: '4', value2: false, style2: "display:none;" },
          { type: 2, value1: '()-2', value2: false, style2: "display:none;" },
          { type: 1, value1: '6', value2: false, style2: "display:none;" }
        ]
      },
      id: "artifact6"
    },
    artifact_7: {
      parent: 'léxicoContent',
      conditions: [

      ],
      engine: DiagramVertical,
      inputs: {
        top: { value: "1", style: "display:none;" },
        bottom: false,
        content: [
          { type: 1, value2: false, value1: '6' },
          { type: 2, value2: false, value1: '9-()' },
          { type: 1, value2: false, value1: '3' },
          { type: 2, value2: false, value1: '5*()' },
          { type: 1, value2: false, value1: '15' }
        ]
      },
      id: "artifact7"
    }
  }
};





const contentMain = new CreateView(allDef);