const def = {

  artifacts: {
    artifac_1: {
      parent: 'tareaContent',
      valuesDefault: [
        {
          type: 2,

          inputA: "6",
          inputB: "3",
          inputC: "4",
          inputD: "",
          inputE: "",
          inputF: "+",
          inputG: "+",
          id: "jsxgraph",
          content: "body",
        },

        {
          type: 1,

          inputA: "6",
          inputB: "3",
          inputC: "4",
          inputD: "",
          inputE: "",
          inputF: "+",
          inputG: "+",
          id: "jsxgraph2",
          content: "body",
        },
      ],
      conditions: [["7"], ["13"], ["9"], ["13"]],
      tmp: "tmp2",
      engine: EngineDiagrama,
    },
    artifac_2: {
      parent: 'tareaContent',
      valuesDefault: [
        {
          type: 2,

          inputA: "8",
          inputB: "4",
          inputC: "5",
          inputD: "",
          inputE: "",
          inputF: "+",
          inputG: "+",
          content: "body",
          id: "jsxgraph3",
        },
      ],
      conditions: [["9"], ["17"]],
      tmp: "tmp",
      engine: EngineDiagrama,
    },
    artifac_3: {
      parent: 'léxicoContent',
      valuesDefault: [
        {
          type: 3,

          inputA: "8",
          inputB: "4",
          inputC: "",
          inputD: "8+4",
          inputE: "+",
          content: "body",
          id: "jsxgraph4",
        },
      ],
      conditions: [["12"]],
      tmp: "tmp",
      engine: EngineDiagrama,
    },
    artifac_4: {
      parent: 'classContent',
      valuesDefault: [
        {
          type: 4,

          inputA: "8",
          inputB: "4",
          inputC: "",
          inputD: "+",
          content: "body",
          id: "jsxgraph5",
        },
      ],
      conditions: [["12"]],
      tmp: "tmp",
      boundingbox: [-5, 8, 5, -3],
      engine: EngineDiagrama,
    },
  },
};

const contentMain = new CreateView(def);
