const defBoards = {
  board_1: {
    artifact: "artifact_1",
    styles: {
      boundingbox: [-4, 3, 4, -2],
      axies: {
        y: { visible: false },
        x: {
          ticks: {
            visible: false
          }
        }
      }
    },
    points: [
      {
        x: 0, y: 0, style: {
          visible: true,
          layer: 100
        }
      },

    ],
    topButtons: [
      { value: 1 },
      { value: 'x2' },
      { value: 3 },
      { value: 'x1' },
      { value: 5 },
      { value: 6 }
    ],
    intervals: [
      {
        height: 1,
        fillInterval: true,
        inputs: {
          a: {
            value: '1',
            style: { mathClass: 'esto' }
          }
        },
      }
    ],

  },
  /////////////////////////
  board_2: {
    artifact: "artifact_1",
    styles: {
      boundingbox: [-4, 3, 4, -2],
      axies: {
        y: { visible: false },
        x: {
          ticks: {
            visible: false
          }
        }
      }
    },
    points: [
      {
        x: 0, y: 0, style: {
          visible: true, color: "red",
          layer: 100
        }
      },

    ],

    intervals: [
      {
        height: 1,
        fillInterval: true,
        inputs: {
          a: {
            value: '3', style: { mathClass: 'esto' }
          },
          b: 4
        },
      }
    ],

  },
};
//si se va a agregar algo al objeto tiene que declararce la propiedad por defecto en el mod.js
const def = {

  artifacts: {
    artifact_1: {
      parent: 'main_2',
      board: 'board_1',
      engine: HorizontalSegment,
      //condiciones
      conditions: {
        keyBoard: true,
        //elementos interval
        intervals: {
          //en los inputs

          inputs: {
            //si falla tiene este mensaje
            text: 'Error desde la def',
            //tiene que tener estos valores
            values: [
              ['x1', 'x2'],
              ['x2']
            ]
          },
        },
      },
      /*   template: { id: 'temp-segment' } */
    },


    artifact_3: {
      board: 'board_2',
      engine: HorizontalSegment,

      conditions: {
        keyBoard: true,
        //elementos interval
        intervals: {
          //en los inputs
          inputs: {
            //si falla tiene este mensaje
            text: 'Error desde la def',
            //tiene que tener estos valores
            values: [
              ['x1', 'x2'],
              ['x2']
            ]
          },
        },
      },
    },
  }
};

const contentMain = new CreateView(def, defBoards);
