describe("Further tests", function () {

  beforeEach(function () {
    jasmine.clock().install();
  });

  afterEach(function () {
    jasmine.clock().uninstall();
  });

  document.getElementsByTagName("body")[0].innerHTML =
    '<div id="jxgbox" style="width: 500px; height: 500px;"></div>';

  const board = JXG.JSXGraph.initBoard("jxgbox", {
    renderer: "svg",
    axis: false,
    grid: false,
    boundingbox: [-5, 5, 5, -5],
    showCopyright: false,
    showNavigation: false
  });

  it("should create a text element with the correct content", function () {
    var el = board.create("text", [0, 10, 'Hello <span class="my">world</span>'], {
      parse: true
    });

    const object = new HorizontalSegment({
      board: 'board_2',
      engine: HorizontalSegment,
      template: { id: 'temp-segment' },
      conditions: {
        keyBoard: true,
        //elementos interval
        intervals: {
          //en los inputs
          inputs: {
            //si falla tiene este mensaje
            text: 'Error desde la def',
            //tiene que tener estos valores
            values: [
              ['x1', 'x2'],
              ['x2']
            ]
          },
        },
      },
    });

    console.log('que es esto', object);
    expect(el.rendNode.innerHTML).toEqual('Hello <span class="my">world</span>');
  });
});