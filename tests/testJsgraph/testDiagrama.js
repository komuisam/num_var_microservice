describe("EngineDiagrama", function () {
  let def = {
    artifacts: {
      artifac_1: {
        valuesDefault: [
          {
            type: 2,

            inputA: "6",
            inputB: "3",
            inputC: "4",
            inputD: "",
            inputE: "",
            inputF: "+",
            inputG: "+",
            id: "jsxgraph",
            content: "body",
          },

          {
            type: 1,

            inputA: "6",
            inputB: "3",
            inputC: "4",
            inputD: "",
            inputE: "",
            inputF: "+",
            inputG: "+",
            id: "jsxgraph2",
            content: "body",
          },
        ],
        conditions: [["7"], ["13"], ["9"], ["13"]],
        tmp: "tmp2",
        engine: EngineDiagrama,
      },
      artifac_2: {
        valuesDefault: [
          {
            type: 2,

            inputA: "8",
            inputB: "4",
            inputC: "5",
            inputD: "",
            inputE: "",
            inputF: "+",
            inputG: "+",
            content: "body",
            id: "jsxgraph3",
          },
        ],
        conditions: [["9"], ["17"]],
        tmp: "tmp",
        engine: EngineDiagrama,
      },
    },
  };
  let fixture = window.__html__["src/book/mobile/view/cap_1/pag_6.html"];
  let engineDiagrama;

  beforeEach(function () {

    document.body.innerHTML = fixture;
    new CreateView(def);/* este test se hace separado por que es la clase create view */

    // Crear una nueva instancia de EngineDiagrama
    engineDiagrama = new EngineDiagrama(def);/* si ya tienes en la definicion el engine ya se estaria creando una instancia del engine y no tiene la capasidad de resivir este tipo de informacion*/
  });

  it("should construct properly", function () {
    expect(engineDiagrama.conditions).toEqual(def.conditions);
    expect(engineDiagrama.artifacts).toEqual(def.artifacts);
    expect(engineDiagrama.valueDefaults).toEqual(def.valuesDefault);
    expect(engineDiagrama.idTemplate).toEqual(def.tmp);
    expect(engineDiagrama.inputsValidate).toEqual([]);
    expect(engineDiagrama.validation).toBeInstanceOf(ValidationDiagrama);
    expect(engineDiagrama.tmp).toEqual(document.querySelector(`#${def.tmp}`));
    expect(engineDiagrama.htmlNode).toEqual(
      engineDiagrama.tmp?.content?.firstElementChild?.cloneNode(true)
    );
    expect(engineDiagrama.contentBoards).toEqual(
      engineDiagrama.htmlNode?.querySelector(".boards")
    );
    expect(engineDiagrama.content).toEqual(document.querySelector("body"));
  });

  it("should return the correct htmlNode", function () {
    /* este test no es valido al 100% tiene que revisar el dom si el template esta entonces el insert si esta funcionando */
    let result = engineDiagrama.templateInsert();

    expect(result).toEqual(engineDiagrama.htmlNode);/* no esta mal pero tiene que revisar el dom tambien */
  });

  it("should reset input fields", () => {
    // Crear un elemento de entrada y agregarlo al htmlNode
    let input = document.createElement("input");
    input.classList.add("show", "passInLibrary", "failedInLibrary");/* no tiene sentido agregarle los estilos desde aqui esto en dado caso tendria que agregarlos el motor */
    input.value = "test";
    engineDiagrama.htmlNode = document.body;
    engineDiagrama.htmlNode.appendChild(input);

    // Llamar al método reset
    engineDiagrama.reset();

    // Verificar que el valor del campo de entrada se ha borrado
    expect(input.value).toEqual("");

    // Verificar que las clases se han eliminado
    expect(input.classList.contains("passInLibrary")).toBe(false);
    expect(input.classList.contains("failedInLibrary")).toBe(false);
  });

  it("should initialize the engine", function () {
    // Crear una nueva instancia de EngineDiagrama
    let engineDiagrama = new EngineDiagrama(def);

    // Espiar los métodos que se llaman en initEngine
    spyOn(engineDiagrama, "initTimer");
    spyOn(engineDiagrama, "boardTypes");
    spyOn(engineDiagrama, "linesPoint");
    spyOn(engineDiagrama, "defineInput").and.returnValue(["testInput"]);

    // Llamar al método initEngine
    engineDiagrama.initEngine();

    // Verificar que se llamaron los métodos esperados
    expect(engineDiagrama.initTimer).toHaveBeenCalledWith(
      engineDiagrama.htmlNode
    );

    // Llama al método boardTypes
    engineDiagrama.boardTypes(
      def.artifacts.artifac_1.valuesDefault[0].type,
      def.artifacts.artifac_1.valuesDefault[0]
    );

    expect(engineDiagrama.boardTypes).toHaveBeenCalledWith(
      def.artifacts.artifac_1.valuesDefault[0].type,
      def.artifacts.artifac_1.valuesDefault[0]
    );

    let mockBoard = { create: jasmine.createSpy() };
    let position = [1, 2];
    let style = { strokecolor: "red" };

    // Llamar al método linesPoint
    engineDiagrama.linesPoint(position, style, mockBoard);

    expect(engineDiagrama.linesPoint).toHaveBeenCalledWith(
      position,
      style,
      mockBoard
    );
  });

  it("should create line correctly", () => {
    let mockBoard = { create: jasmine.createSpy() };
    let position = [1, 2];
    let style = { strokecolor: "red" };

    // Llamar al método linesPoint
    engineDiagrama.linesPoint(position, style, mockBoard);

    // Verificar que board.create se llamó con los argumentos correctos
    expect(mockBoard.create).toHaveBeenCalledWith("line", position, {
      strokecolor: "blue",
      strokeWidth: 2,
      straightFirst: false,
      straightLast: false,
      fixed: true,
      ...style,
    });
  });

  it("should define input correctly", () => {
    let mockInputs = ["mockInput"];
    spyOn(engineDiagrama, "createInputs").and.returnValue(mockInputs);

    let x = 1,
      y = 2,
      text = "test",
      type = 3;

    // Llamar al método defineInput
    let result = engineDiagrama.defineInput(x, y, text, type);

    // Verificar que createInputs se llamó con los argumentos correctos
    expect(engineDiagrama.createInputs).toHaveBeenCalledWith({
      inputs: [
        {
          x,
          y,
          value: text,
          style: {
            mathClass: "inputXpansion",
            mathRest: " ",
          },
        },
      ],
    });

    // Verificar que defineInput devuelve el valor correcto
    expect(result).toEqual(mockInputs);
  });
});
