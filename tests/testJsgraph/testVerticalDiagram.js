// const { it, describe } = require("node:test");
describe('test for VerticalDiagram for p_54', function () {
  const engine = new DiagramVertical({
    conditions: [
      { name: "up", value: ["1", "2"] },
      { name: "f1c2", value: "2" },
      { name: "down", value: "3" },
      { name: "f2c2", value: "4" },
      { name: "f3c1", value: "5" },
      { name: "f3c2", value: "6" },
      { name: "f4c2", value: "7" },
      { name: "f5c1", value: "8" }

    ],
    engine: DiagramVertical,
    inputs: {

      bottom: { value: "2" },
      content: [
        { type: 1, value1: 'x' },
        { type: 2, value1: '()+3' },
        { type: 1 },
        { type: 2, value1: '2*()' },
        { type: 1, value2: '20' }
      ]
    },
    id: "artifact1"
  }
  );

  it("test for setInputs p_54", function () {/* ok */

    const inputExample = document.createElement("input");

    engine.setInput(inputExample, "color: red", "hola carlos");

    expect(inputExample.style.color).toBe("red");
    expect(inputExample.textContent).toBe("hola carlos");
    expect(inputExample.disabled).toBe(true);

  });

  it("test for initMainReset p_54", function () {/* ok */

    const inputReset = document.createElement("input");

    engine.inputsValidate.push(inputReset);

    inputReset.value = "helouquis";
    inputReset.style.background = "black";

    engine.iniTMainReset();

    expect(inputReset.value).toBe("");
    expect(inputReset.style.background).toBe("transparent");

  });

  it('test for template insert p_54', function () {/* ladron!! */
    expect(engine.templateInsert())
      .toEqual(document.querySelector('#temp').content.firstElementChild);
  })

})
