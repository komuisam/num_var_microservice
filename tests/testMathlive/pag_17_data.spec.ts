import type {MathfieldElement} from "../mathlive/mathfield-element";
import {test, expect} from "@playwright/test";
import "dotenv/config";

const artifacts = [
  {id: "artifact_1", correcta: "", inCorrecta: "", forAswer: ""},
  {id: "artifact_2", correcta: "", inCorrecta: "", forAswer: ""},
  {id: "artifact_3", correcta: "", inCorrecta: "", forAswer: ""},
  {id: "artifact_4", correcta: "", inCorrecta: "", forAswer: ""},
  {id: "artifact_5", correcta: "", inCorrecta: "", forAswer: ""},
  {id: "artifact_6", correcta: "", inCorrecta: "", forAswer: ""},
  {id: "artifact_7", correcta: "", inCorrecta: "", forAswer: ""},
  {id: "artifact_8", correcta: "", inCorrecta: "", forAswer: ""},
  {id: "artifact_9", correcta: "", inCorrecta: "", forAswer: ""},
  {id: "artifact_10", correcta: "", inCorrecta: "", forAswer: ""},
];

const values = ["23", "42", "12", "3", "8"];

test("test verify data", async ({page}) => {
  await page.goto(
    `${process.env.SERVERHOST}/src/book/mobile/view/cap_1/pag_17.html`,
  );

  for (let i = 0; i < artifacts.length; i++) {
    const artifact = artifacts[i];
    const selects = await page.locator(`#${artifact.id} select`).all();
    let mathFields = await page.locator(`#${artifact.id} .mathfield`).all();

    for (let i = 0; i < mathFields.length; i++) {
      const value = values[i];
      await mathFields[i].evaluate((mathfield: MathfieldElement, value) => {
        const setValue = (mfe: MathfieldElement, val) => {
          mfe.value = val;
          return mfe.value;
        };

        setValue(mathfield, value);
        return mathfield.value;
      }, value);
    }

    for (const selectElement of selects) {
      await selectElement.selectOption({index: 2});
    }

    await page.locator(`#${artifact.id} .check`).click();
    const correctas = await page.locator(".modalGeneric-content .correct");
    const inCorrect = await page.locator(".modalGeneric-content .inCorrect");
    const forAswer = await page.locator(".modalGeneric-content .forAswer");

    artifact.correcta = (await correctas.textContent()) ?? "";
    artifact.inCorrecta = (await inCorrect.textContent()) ?? "";
    artifact.forAswer = (await forAswer.textContent()) ?? "";

    await expect(correctas).toHaveText(`${artifact.correcta}`);
    await expect(inCorrect).toHaveText(`${artifact.inCorrecta}`);
    await expect(forAswer).toHaveText(`${artifact.forAswer}`);
    await page.locator(".modalGenericClose").click();
  }
});
