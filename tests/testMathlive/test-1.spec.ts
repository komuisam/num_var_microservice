import {test, expect} from "@playwright/test";

test("test", async ({page}) => {
  await page.goto(
    "http://127.0.0.1:5501/src/book/mobile/view/cap_1/pag_54.html",
  );
  await page.locator(".ML__content").first().click();
  await page.locator(".ML__keyboard-sink").first().fill("c");
  await page.locator("#artifact1").getByRole("button", {name: "✓"}).click();
});
